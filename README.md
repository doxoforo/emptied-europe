# Emptied Europe

Think Tank de la España depauperada, para crear recursos para cualquier territorio del mundo que quiera compartir recursos con los demás.

Think Tank of impoverished Spain, to create resources for any territory in the world that wants to share theese resources with others. 

## Cómo funciona?

En este repositiorio irá evolucionando la estructura del think tank que es un contenedor de contenedores. Como tal debe desarrollar su propia sistema procedimental interno, desde que llega una idea semilla hasta que se convierte en un recurso solvente.

El primer contenedor será un Partido Instrumental completo con el que cualquiera pueda presentarse a unas elecciones en su comunidad sin caer en todos los errores y carencias que no puedes preveer hasta que dichos problemas superan la iulsión inicial por la cual pensaste que era una buena idea comprometerte con tu municipio, aldea, provincia o región.

Este repositiorio no va a guardar ninguna documentación de sofware.  Solo una wiki donde recoger todos los enlaces a todos los recursos e ideas que irán naciendo.
Se podría decir que será la wikipedia del think tank donde encontrar todo aquello que existe.  Y por tanto, si no lo encuentras es que aun no existe y necesitamos que lo propongas tú.

Comienza en nuestra página [wiki](Wiki/main.md)

